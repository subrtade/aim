import numpy as np
import matplotlib.pyplot as plt
import utils
import cv2

# image_name = 'mona_lisa.png'
# image_name = 'M.jpg'
image_name = 'balloons.jpg'
SHOW_PLOTS =False

im = cv2.imread('./images/'+image_name,cv2.IMREAD_GRAYSCALE)
assert im is not None, "Image not found"
im = utils.im2double(im)
if SHOW_PLOTS:
    bins,counts = utils.get_hist(im,256)
    utils.plot_hist(bins,counts)

increase_contrast = lambda x: x * 0.75
im2 = utils.transform_by_func(increase_contrast, im)
if SHOW_PLOTS:
    bins2,counts2 = utils.get_hist(im2,256)
    utils.plot_hist(bins2,counts2)
cv2.imwrite('./images/transformed_contrast_'+image_name,utils.im2uint(im2))


increase_brightness = lambda x: x + 0.25
im3 = utils.transform_by_func(increase_brightness, im)
if SHOW_PLOTS:
    bins3,counts3 = utils.get_hist(im3,256)
    utils.plot_hist(bins3,counts3)
cv2.imwrite('./images/transformed_brightness_'+image_name,utils.im2uint(im3))

create_negative = lambda x: 1-x
im4 = utils.transform_by_func(create_negative, im)
if SHOW_PLOTS:
    bins4,counts4 = utils.get_hist(im4,256)
    utils.plot_hist(bins4,counts4)
cv2.imwrite('./images/transformed_negative_'+image_name,utils.im2uint(im4))


alpha = 0.6
gamma = 1/ (1-alpha)
non_lin_contrast_lth = lambda x: 0.5*(2*x)**gamma
non_lin_contrast_gth = lambda x: 1 - 0.5*(2-2*x)**gamma
im5_lth = utils.transform_by_func(non_lin_contrast_lth, np.multiply(im,im<0.5))
im5_gth = utils.transform_by_func(non_lin_contrast_gth, np.multiply(im,im>=0.5))
im5 = np.multiply(im5_lth,im<0.5) + np.multiply(im5_gth,im>=0.5)
if SHOW_PLOTS:
    bins5,counts5 = utils.get_hist(im5,256)
    utils.plot_hist(bins5,counts5)
cv2.imwrite('./images/transformed_non_lin_contrast_'+image_name,utils.im2uint(im5))



# bins4, counts4 = utils.get_hist(im, 256)
# utils.plot_hist(bins4, counts4)
# cdf = utils.cdf_from_hist(counts4)
# plt.plot(cdf)
# plt.show()
fin_im = utils.hist_eq(im)
# bins4, counts4 = utils.get_hist(fin_im, 256)
# utils.plot_hist(bins4, counts4)
# cdf = utils.cdf_from_hist(counts4)
# plt.plot(bins4,cdf)
# plt.show()
cv2.imwrite('./images/transformed_hist_equalization_'+image_name,utils.im2uint(fin_im))

# plt.imshow(fin_im)
# plt.gray()
# plt.show()