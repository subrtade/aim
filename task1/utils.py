from matplotlib import pyplot as plt
import numpy as np

def im2double(im):
    return im/255.

def im2uint(im):
    return im*255

def get_hist(im, n_bins=256):
    bin_centers = np.linspace(0,1,n_bins)
    pixels = im.reshape(-1)
    # bin_ind = list(map(lambda x: np.argmin(np.abs(x-bin_centers)),pixels))
    # "readable"
    bin_ind = []
    for px in pixels:
        # argument of the closest bin
        bin_of_px = np.argmin(np.abs(px-bin_centers))
        bin_ind.append(bin_of_px)
    # accumulate counts to each bin
    ids,counts = np.unique(bin_ind,return_counts=True)
    return ids,counts

def transform_by_func(func, im):
    im = func(im)
    im = np.clip(im,0,1)
    return im

def cdf_from_hist(counts):
    counts_cumsum = np.cumsum(counts)
    return counts_cumsum/counts_cumsum[-1]

def hist_eq(im):
    im_as_inds = np.uint8(im2uint(im))
    n_bins = 256
    ids,counts = get_hist(im,n_bins)
    cdf = cdf_from_hist(counts)
    # equal_hist_im = np.uint8(cdf[im_as_inds]*255)
    equal_hist_im = cdf[im_as_inds]
    return equal_hist_im




def plot_hist(bins, counts):
    plt.bar(bins,counts)
    plt.show()

