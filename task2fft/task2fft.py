import matplotlib.pyplot as plt
import numpy as np
import utils
import cv2


image_name = 'M.jpg'
# image_name = 'balloons.jpg'
# image_name = 'mona_lisa.png'
SHOW_FIGURES = False

im = cv2.imread('./images/' + image_name, cv2.IMREAD_GRAYSCALE)
assert im is not None, "Image not found"
im = utils.im2double(im)
imsiz = im.shape

plt.gray()
circle_params = (int(imsiz[0] / 2), int(imsiz[1] / 2), 10)
# invert = True for high-pass filter, invert = False for low-pass
mask = utils.circular_mask(imsiz, circle_params, invert=True)
# fft
im_fft = np.fft.fft2(im)
im_fft = np.fft.fftshift(im_fft)
# apply filter
im_fft_transformed = np.multiply(im_fft, mask)
# inverse fft
im_transformed = np.fft.ifftshift(im_fft_transformed)
im_transformed = np.real(np.fft.ifft2(im_transformed))
if SHOW_FIGURES:
    plt.imshow(im_transformed)
    plt.show()

cv2.imwrite('./images/transformed_' + image_name, utils.im2uint(im_transformed))

# plt.imshow(np.angle(im_fft))
if SHOW_FIGURES:
    plt.imshow(np.log(np.abs(im_fft)))
    plt.colorbar()
    plt.show()
im_2save_fft = utils.im2double(np.log(np.abs(im_fft)))
cv2.imwrite('./images/spectrum_original_image_' + image_name, utils.im2uint(im_2save_fft))
# non-lin contrast
alfa = 0.9
gamma = 1 / (1 - alfa)
im_lth = 0.5 * (2 * im) ** gamma
im_gth = 1 - 0.5 * (2 - 2 * im) ** gamma
im_nonlin = im
im_nonlin[im < 0.5] = im_lth[im < 0.5]
im_nonlin[im >= 0.5] = im_gth[im >= 0.5]
if SHOW_FIGURES:
    plt.imshow(im_nonlin)
    plt.colorbar()
    plt.show()
im_2save_fft = np.fft.fft2(im_nonlin)
im_2save_fft = np.fft.fftshift(im_2save_fft)
# get magnitude by comp abs value of complex number
im_2save_fft = utils.im2double(np.log(np.abs(im_2save_fft)))
cv2.imwrite('./images/spectrum_nonlinear_contrast_' + image_name, utils.im2uint(im_2save_fft))

# log scale
s = 2
im_logscale = np.clip(np.divide(np.log(1 + im * s), np.log(1 + s)), 0, 1)
if SHOW_FIGURES:
    plt.imshow(im_logscale)
    plt.colorbar()
    plt.show()
im_2save_fft = np.fft.fft2(im_logscale)
im_2save_fft = np.fft.fftshift(im_2save_fft)
# get magnitude by comp abs value of complex number
im_2save_fft = utils.im2double(np.log(np.abs(im_2save_fft)))
cv2.imwrite('./images/spectrum_logscale_' + image_name, utils.im2uint(im_2save_fft))
# threshold
im_thr = np.uint8(im >= 0.35)
if SHOW_FIGURES:
    plt.imshow(im_thr)
    plt.colorbar()
    plt.show()
im_2save_fft = np.fft.fft2(im_thr)
im_2save_fft = np.fft.fftshift(im_2save_fft)
# get magnitude by comp abs value of complex number
im_2save_fft = utils.im2double(np.log(np.abs(im_2save_fft)))
cv2.imwrite('./images/spectrum_threshold_' + image_name, utils.im2uint(im_2save_fft))

# im_bright = np.clip(np.power(im,2),0,1)
# im_bright = np.clip(im*0.65,0,1)
# im_bright = np.clip(im-0.25,0,1)
# im_bright = np.clip(1-im,0,1)
# im_bright = np.floor(im*32)

# mask_fft = np.fft.fft2(mask)
# mask_fft = np.fft.fftshift(mask_fft)
# plt.imshow(np.log(np.abs((mask_fft))))
# plt.show()
