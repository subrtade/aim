import numpy as np

def circular_mask(size,circle,invert=False):
	assert type(size) is tuple, "size should be a tuple (width,height)"
	assert type(circle) is tuple, "parameters of a circle should be a tuple"
	# assert circle[0]>=0 and circle[1]>=0, "circle isn't in the image"
	assert circle[2] > 0, 'radius is negative'
	center_x = circle[1]
	center_y = circle[0]
	r = circle[2]
	x = np.arange(0,size[1],1)
	y = np.arange(0,size[0],1)
	xx,yy = np.meshgrid(x,y)
	distances = np.sqrt((xx-center_x)**2 + (yy-center_y)**2)
	mask = distances <= r
	if invert:
		mask = np.logical_not(mask)
	return mask

def im2double(im):
	return im/np.max(im)

def im2uint(im):
	return im*255
