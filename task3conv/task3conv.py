import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage.filters import gaussian_filter
import utils
import cv2

# image_name = 'M.jpg'
# image_name = 'mona_lisa.png'
image_name = 'balloons.jpg'
out_name = image_name.split('.')[0]+'_filtered'

im = cv2.imread('./images/'+image_name,cv2.IMREAD_GRAYSCALE)
assert im is not None, "Image not found"
im = utils.im2double(im)
imsiz = im.shape

sigmas = np.arange(0.5,3.5,0.5)
SHOW_FIGURES = True

for sigma in sigmas:
    conv_im = utils.filter_im_w_gauss_2d(im,sigma)
    # scale to [0,1]
    conv_im = conv_im- np.min(conv_im)
    conv_im = conv_im/np.max(conv_im)
    # ref
    # conv_ref = gaussian_filter(im,sigma)
    cv2.imwrite('./images/'+out_name+'_sigma'+str(sigma)+'.png',utils.im2uint(conv_im))
    if SHOW_FIGURES:
        plt.imshow(conv_im)
        plt.gray()
        plt.show()
        # plt.imshow(conv_ref)
        # plt.gray()
        # plt.show()

