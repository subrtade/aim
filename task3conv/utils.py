import numpy as np


def im2double(im):
    return im / 255.


def im2uint(im):
    return im * 255


def gauss(x, sigma):
    res = 1 / (np.sqrt(2 * np.pi) * sigma) * np.exp(-0.5 * (np.multiply(x, x) / sigma ** 2))
    return res


def gauss_filter(sigma):
    x = np.arange(-np.ceil(3 * sigma), np.ceil(3 * sigma), 1)
    gauss_filter = gauss(x, sigma)
    gauss_filter = gauss_filter / np.sum(gauss_filter)
    return gauss_filter.reshape(-1, 1)


def conv(img, conv_filter):
    filtersiz = conv_filter.shape
    f_len = filtersiz[0]
    if filtersiz[0] == 1:
        f_len = filtersiz[1]
        img = img.T
    conv_filter = np.flip(conv_filter).reshape(-1)
    f_center = int(np.ceil(f_len / 2))
    new_im = np.zeros_like(img)
    for irow in range(img.shape[0]):
        for icol in range(img.shape[1]):
            # left edge of the image
            if icol < f_center:
                new_im[irow, icol] = np.sum(img[irow, :(icol + f_center)] * conv_filter[(f_center - icol):])
                continue
            # right edge
            if icol > (img.shape[1] - f_center):
                pixels_to_end = img.shape[1] - icol
                new_im[irow, icol] = np.sum(img[irow, (icol - f_center):] * conv_filter[:(pixels_to_end + f_center)])
                continue
            # inside the image
            new_im[irow, icol] = np.sum(img[irow, (icol - f_center):(icol + f_center)] * conv_filter)
    if filtersiz[0] == 1:
        new_im = new_im.T
    return new_im


def filter_im_w_gauss_2d(im, sigma):
    filter_x = gauss_filter(sigma)
    filter_y = filter_x.T
    conv_y = conv(im, filter_y)
    conv_xy = conv(conv_y, filter_x)
    return conv_xy
