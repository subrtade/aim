#include <iostream>
#include <math.h>
#include <vector>
#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"


using namespace std;

double gauss1d(double x, double sigma) {
    return exp(-0.5 * ((x * x) / (sigma * sigma)));
}

cv::Mat filter_bilateral(cv::Mat img, double sigmaG, double sigmab) {
    cv::Mat kernel = cv::getGaussianKernel((int) 2 * ceil(3 * sigmaG), sigmaG, CV_64F);
    cv::Mat kernel2d = kernel * kernel.t();
    double maxV = 0;
    cv::minMaxIdx(kernel2d, NULL, &maxV);
    kernel2d = (kernel2d / maxV);
    int type_filt = CV_64FC1;
    int channels = img.channels();
    if (channels == 3) {
        type_filt = CV_64FC3;
    }
    cv::Mat filtered = cv::Mat::zeros(img.rows, img.cols, type_filt);
    double r = 0;
    double res = 0;
    double w = 0;
    int gf_center_x = (int) ceil(kernel2d.rows / 2);
    int gf_center_y = (int) ceil(kernel2d.cols / 2);
    for (int s = 0; s < img.rows; ++s) {
        for (int t = 0; t < img.cols; ++t) {
            for (int ich = 0; ich < channels; ++ich) {
                res = 0;
                w = 0;
                for (int irow = -gf_center_x; irow < gf_center_x; ++irow) {
                    for (int icol = -gf_center_y; icol < gf_center_y; ++icol) {
                        if ((s + irow) >= 0 && (t + icol) >= 0 && (s + irow) < img.rows && (t + icol) < img.cols) {
                            r = kernel2d.data[kernel2d.cols * (gf_center_x + irow) + gf_center_y + icol] * gauss1d(
                                    img.data[channels * img.cols * (s + irow) + channels * (t + icol) + ich] -
                                    img.data[channels * img.cols * s + channels * t + ich], sigmab);
                            res += img.data[channels * img.cols * (s + irow) + channels * (t + icol) + ich] * r;
                            w += r;
                        }
                    }
                }
                filtered.at<cv::Vec3d>(s, t)[ich] = res / w;
            }
        }
    }

    return filtered;
}


int main() {
    string path = "/Users/subrtade/Documents/FEL/L20/AIM/bilateral_cpp/";
    string fname = "taj.jpg";
    cv::Mat img = cv::imread(path + fname, cv::IMREAD_COLOR);
    vector<double> sigmasG = {5, 7, 9, 10};
    vector<double> sigmasb = {5, 10, 20, 30, 50, 100, 300};

    int type_filt = CV_32FC1;
    if (img.channels() == 3) {
        type_filt = CV_32FC3;
    }
    cv::Mat res = cv::Mat(img.size(), type_filt);
    double minV;
    double maxV;
    for (double sG: sigmasG) {
        for (double sb: sigmasb) {
            res = filter_bilateral(img, sG, sb);
            cv::minMaxIdx(res, &minV, &maxV);
            cout << maxV << "  " << minV << endl;
            cv::Mat to_save = cv::Mat(img.size(), CV_8U);
            to_save = 255 * (res - minV) / (maxV - minV);
            string name = "filtered_" + fname + "(" + to_string(sG) + "," + to_string(sb) + ")" + ".png";
            cv::imwrite(name, to_save);
        }
    }

    cout << "Hello, World!" << endl;
    return 0;
}