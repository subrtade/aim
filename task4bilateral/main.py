import cv2
import utils
from scipy.ndimage.filters import gaussian_filter
import numpy as np
import matplotlib.pyplot as plt

SHOW_FIGURES = True

# image_name = 'lena_noisy.jpg'  # [1,0.1]
# image_name = 'taj.jpg'  # [1,0.1]
# image_name = 'mountain.png' # [1,0.1]
# image_name = 'balloons.jpg'
# image_name = 'baboon.png'
# image_name = 'airplane.png' # [1,0.1]
# image_name = 'barbara.png' # [1,0.01]
image_name = 'cameraman.png'
out_name = image_name.split('.')[0] + '_filtered'

im = utils.bgr2rgb(cv2.imread('./images/' + image_name))
assert im is not None, "Image not found"
# im = utils.im2double(im)

# scale = 0.25
scale = 1
# scale = 0.5
new_siz = tuple(map(lambda x: int(x * scale), im.shape[:2]))
im = cv2.resize(im, new_siz)

# cv2.imwrite('./images/' + out_name + '_original.png', utils.im2uint(im))
# sigmasG = np.arange(0.5, 3.5, 0.5)
sigmasG = [3, 5, 10, 15, 30]
sigmasb = [1, 5, 10, 20, 30, 50, 100, 300]
for sG in sigmasG:
    for sb in sigmasb:
         filtered = utils.filter_bilateral(im, sG, sb)
        cv2.imwrite(f'./images/{out_name}_({sG},{sb}).png', utils.bgr2rgb(utils.imNorm(filtered)))
        if SHOW_FIGURES:
            plt.imshow(filtered)
            plt.gray()
            plt.show()

# gausFil = gaussian_filter(im, 1.)
# plt.imshow(gausFil)
# plt.gray()
# plt.show()
#
# plt.imshow(im - filtered)
# plt.gray()
# plt.show()
