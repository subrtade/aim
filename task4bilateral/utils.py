import numpy as np
from scipy.ndimage.filters import gaussian_filter, convolve


def im2double(im):
    return im / 255.


def bgr2rgb(im):
    return im[:, :, [2, 1, 0]]


def im2uint(im):
    return im * 255


def imNorm(im):
    im = im - np.min(im)
    im = im / np.max(im)
    return im2uint(im)


def gauss2d(x, y, sigma):
    res = 1 / (np.sqrt(2 * np.pi) * sigma) * np.exp(-0.5 * ((x * x + y * y) / sigma ** 2))
    return res


# default type float32 instead of float 64
def array(*args, **kwargs):
    kwargs.setdefault("dtype", np.float32)
    return np.array(*args, **kwargs)


def gauss1d(x, sigma):
    res = 1 / (np.sqrt(2 * np.pi) * sigma) * np.exp(-0.5 * (np.multiply(x, x) / sigma ** 2))
    return res


def gauss_filter2d(sigma):
    points1D = array(np.arange(-np.ceil(3 * sigma), np.ceil(3 * sigma), 1))
    xx, yy = np.meshgrid(points1D, points1D)
    gauss_filter = array(gauss2d(xx, yy, sigma))
    # gauss_filter = gauss_filter / np.sum(gauss_filter)
    return gauss_filter


def gauss_filter1d(sigma):
    x = np.arange(-np.ceil(3 * sigma), np.ceil(3 * sigma), 1)
    gauss_filter = gauss1d(x, sigma)
    # gauss_filter = gauss_filter / np.sum(gauss_filter)
    return gauss_filter.reshape(-1, 1)


def filter_bilateral(im, sigmaG, sigmab):
    gauss_filter = gauss_filter2d(sigmaG)
    # print(gauss_filter.shape)
    gf_center_x = int(np.ceil(gauss_filter.shape[0] / 2))
    gf_center_y = int(np.ceil(gauss_filter.shape[1] / 2))
    b = lambda x: gauss1d(x, sigmab)
    filtered = np.zeros_like(im)
    for s in range(im.shape[0]):
        for t in range(im.shape[1]):
            res = 0
            w = 0
            for irow in range(-gf_center_x, gf_center_x):
                for icol in range(-gf_center_y, gf_center_y):
                    if (s + irow) >= 0 and (t + icol) >= 0 and (s + irow) < im.shape[0] and (t + icol) < im.shape[1]:
                        # print(f'({irow},{icol}) -> [{s+irow},{t+icol}] -> [{gf_center_x+irow},{gf_center_y+icol}]')
                        res += im[s + irow, t + icol, :] * gauss_filter[gf_center_x + irow, gf_center_y + icol] * b(
                            im[s + irow, t + icol] - im[s, t])
                        w += gauss_filter[gf_center_x + irow, gf_center_y + icol] * b(
                            im[s + irow, t + icol] - im[s, t])
                        # print(res)
            filtered[s, t] = res / w

    return filtered


def conv1d(img, conv_filter):
    filtersiz = conv_filter.shape
    f_len = filtersiz[0]

    if filtersiz[0] == 1:
        f_len = filtersiz[1]
        img = img.T
    conv_filter = np.flip(conv_filter).reshape(-1)
    f_center = int(np.ceil(f_len / 2))
    new_im = np.zeros_like(img)
    for irow in range(img.shape[0]):
        for icol in range(img.shape[1]):
            # left edge of the image
            if icol < f_center:
                new_im[irow, icol] = np.sum(img[irow, :(icol + f_center)] * conv_filter[(f_center - icol):])
                continue
            # right edge
            if icol > (img.shape[1] - f_center):
                pixels_to_end = img.shape[1] - icol
                new_im[irow, icol] = np.sum(
                    img[irow, (icol - f_center):] * conv_filter[:(pixels_to_end + f_center)])
                continue
            # inside the image
            new_im[irow, icol] = np.sum(img[irow, (icol - f_center):(icol + f_center)] * conv_filter)
    if filtersiz[0] == 1:
        new_im = new_im.T
    return new_im


def filter_bilateral_piecewise_linear(im, sigmaG, sigmab, intensity):
    b = lambda x: gauss1d(x, sigmab)
    intensity_weights = b(im - intensity)
    im_ = im * intensity_weights
    w_const = gaussian_filter(intensity_weights, sigmaG)
    conv_xy = gaussian_filter(im_, sigmaG)
    conv_xy = conv_xy / w_const
    # my functions are slow
    # filter_x = gauss_filter1d(sigmaG)
    # filter_y = filter_x.T
    # conv_y = conv1d(im_, filter_y)
    # conv_xy = conv1d(conv_y, filter_x)
    return conv_xy


def bilateral_approx(im, sigmaG, sigmab, n_intensities=16):
    im_siz = im.shape
    intensities = np.uint(np.linspace(np.min(im), np.max(im), n_intensities))
    closest_lower = lambda q_int: np.argwhere(intensities >= q_int)[0][0] - 1
    out = np.zeros_like(im)
    approx = np.zeros((len(intensities), np.prod(out.shape)))
    for iint, tone in enumerate(intensities):
        i = filter_bilateral_piecewise_linear(im, sigmaG, sigmab, tone)
        approx[iint] = i.reshape(-1)
    out = out.reshape(-1)

    for ind_, px in enumerate(im.reshape(-1)):
        lower_saved_id = closest_lower(px)
        lower_saved_int = intensities[lower_saved_id]
        higher_saved_int = intensities[lower_saved_id + 1]
        coeff_1 = (px - lower_saved_int) / (higher_saved_int - lower_saved_int)
        out[ind_] = coeff_1 * approx[lower_saved_id, ind_] + (1 - coeff_1) * approx[lower_saved_id + 1, ind_]
    out = out.reshape(im_siz)
    return out


def bilateral_approx_rgb(im, sigmaG, sigmab, n_intensities=16):
    im_out = np.zeros_like(im)
    for ch_i in range(im.shape[2]):
        im_out[:, :, ch_i] = bilateral_approx(im[:, :, ch_i], sigmaG, sigmab, n_intensities)
    return im_out
