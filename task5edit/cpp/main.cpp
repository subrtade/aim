#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;

cv::Mat im4saving(cv::Mat im) {
    double min;
    double max;
    cv::minMaxIdx(im, &min, &max);
    im = 255 * (im - min) / (max - min);
    return im;
}


cv::Mat gauss_seidel(cv::Mat img_0, cv::Mat div, cv::Mat img_bound, int start_row,int end_row, int start_col,int end_col) {
//    cv::Mat img_0 = cv::Mat::zeros(img.rows,img.cols,img.type());
    int iter_ = 0;
    double sum_ = 0;
    while (iter_ < 10) {
        for (int ir = start_row; ir < end_row; ++ir) {
            for (int ic = start_col; ic < end_col; ++ic) {
                for (int ch = 0; ch < img_0.channels(); ++ch) {
                    if (ir == start_row || ir == end_row - 1 || ic == start_col || ic == end_col - 1) {
                        sum_ = 0;
//                        top
                        if (ir == start_row) {
                            sum_ += (float)img_bound.at<cv::Vec3b>(start_row, ic)[ch];
                        } else {
                            sum_ += (float)img_0.at<cv::Vec3b>(ir - 1, ic)[ch];
                        }
//                        left edge
                        if (ic == start_col) {
                            sum_ += (float)img_bound.at<cv::Vec3b>(ir, start_col)[ch];
                        } else {
                            sum_ += (float)img_0.at<cv::Vec3b>(ir, ic - 1)[ch];
                        }
//                        bottom
                        if (ir == end_row - 1) {
                            sum_ += (float)img_bound.at<cv::Vec3b>(end_row- 1, ic)[ch];
                        } else {
                            sum_ += (float)img_0.at<cv::Vec3b>(ir + 1, ic)[ch];
                        }
//                        right edge
                        if (ic == end_col - 1) {
                            sum_ += (float)img_bound.at<cv::Vec3b>(ir, end_col - 1)[ch];
                        } else {
                            sum_ += (float)img_0.at<cv::Vec3b>(ir, ic + 1)[ch];
                        }
                        img_0.at<cv::Vec3b>(ir, ic)[ch] = (float)(0.25) * (sum_ - (float)div.at<cv::Vec3b>(ir, ic)[ch]);
                    }else {
                        img_0.at<cv::Vec3b>(ir, ic)[ch] = (float) (0.25) * (
                                (float) img_0.at<cv::Vec3b>(ir + 1, ic)[ch] +
                                (float) img_0.at<cv::Vec3b>(ir - 1, ic)[ch] +
                                (float) img_0.at<cv::Vec3b>(ir, ic - 1)[ch] +
                                (float) img_0.at<cv::Vec3b>(ir, ic + 1)[ch] -
                                (float) div.at<cv::Vec3b>(ir, ic)[ch]);
                    }
                }
            }

        }
        cout<< "iter #"<<iter_<<endl;

        cv::imwrite("/Users/subrtade/Documents/FEL/L20/AIM/aim/task5edit/images/iter"+to_string(iter_)+".jpg", im4saving(img_0));
        iter_++;

    }
    return img_0;
}



cv::Mat im_dx_dy(cv::Mat im, bool diff_dx) {
    cv::Mat sob_1 = cv::Mat({-1, 0, 1});
    cv::Mat sob_2 = cv::Mat({1, 2, 1});
    cv::Mat d;
    if (diff_dx) {
        cv::sepFilter2D(im, d, -1, sob_2, sob_1);
    } else {
        cv::sepFilter2D(im, d, -1, sob_1, sob_2);
    }
    cout<<"im_dx_dy"<<endl;
    return d;
}


int main() {
    std::cout << "Hello, World!" << std::endl;
    string path = "/Users/subrtade/Documents/FEL/L20/AIM/aim/task5edit/images/";
    string fnameA = "ruka.jpg";
    string fnameB = "oko.jpg";
    cv::Mat imA = cv::imread(path + fnameA, cv::IMREAD_COLOR);
    cv::Mat imB = cv::imread(path + fnameB, cv::IMREAD_COLOR);
    cout << imA.size() << endl;
    cout << imB.size() << endl;

//    stitch gradients
    cv::Mat dx1 = im_dx_dy(imA, true);
    cv::Mat dy1 = im_dx_dy(imA, false);
    cv::Mat dx2 = im_dx_dy(imB, true);
    cv::Mat dy2 = im_dx_dy(imB, false);
    for (int i = 0; i < imB.rows; ++i) {
        for (int j = 0; j < imB.cols; ++j) {
            dx1.at<cv::Vec3b>(1900 + i, 1300 + j)[0] = (int)dx2.at<cv::Vec3b>(i, j)[0];
            dx1.at<cv::Vec3b>(1900 + i, 1300 + j)[1] = (int)dx2.at<cv::Vec3b>(i, j)[1];
            dx1.at<cv::Vec3b>(1900 + i, 1300 + j)[2] = (int)dx2.at<cv::Vec3b>(i, j)[2];
            dy1.at<cv::Vec3b>(1900 + i, 1300 + j)[0] = (int)dy2.at<cv::Vec3b>(i, j)[0];
            dy1.at<cv::Vec3b>(1900 + i, 1300 + j)[1] = (int)dy2.at<cv::Vec3b>(i, j)[1];
            dy1.at<cv::Vec3b>(1900 + i, 1300 + j)[2] =(int) dy2.at<cv::Vec3b>(i, j)[2];
        }
    }
//
    cv::imwrite(path + "dx1.jpg", im4saving(dx1));
    cv::imwrite(path + "dy1.jpg", im4saving(dy1));
//    compute divergence matrix
    cv::Mat div = im_dx_dy(dx1, true);
    cv::Mat gdy = im_dx_dy(dy1, false);
    div+= gdy;
    cv::imwrite(path + "div.jpg", im4saving(div));
//    gauss-seidel on a subset of the image
    cv::Mat out = gauss_seidel(imA, div, imA, 1800, 2000+imB.rows, 1200,1400 + imB.cols);
//    cv::Mat out = gauss_seidel(imA, div, imA, 0, imA.rows, 0,imA.cols);
    cv::imwrite(path + "result.jpg", im4saving(out));
    return 0;
}