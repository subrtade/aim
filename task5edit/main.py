import matplotlib.pyplot as plt
from scipy.ndimage import convolve1d, convolve, gaussian_filter
import numpy as np
import cv2
import os


def gauss_seidel(img1, img2, mask, n_iters=30, start_r=0, start_c=0, end_r=None, end_c=None):
    div = compose_div(img1, img2, mask)
    # default setting iterates over the whole image
    if end_r is None:
        end_r = img1.shape[0]
    if end_c is None:
        end_c = img1.shape[1]
    # div = compose_div_hand(img1, img2)
    img_0 = img1 * np.ones_like(img1)
    # out = 255 * (img_0 - np.min(img_0)) / (np.max(img_0) - np.min(img_0))
    out = img_0
    out = out[:, :, [2, 1, 0]]
    cv2.imwrite(f'kluk_init.png', out)
    iter_ = 0
    sum_ = 0
    while iter_ < n_iters:
        for ir in range(start_r, end_r):
            for ic in range(start_c, end_c):
                # border
                if (ir == start_r) or ir == (end_r - 1) or ic == start_c or ic == (end_c - 1):
                    sum_ = 0
                    # out of bounds top
                    if ir == start_r:
                        # boundary conditions taken from img2
                        sum_ += img1[start_r, ic]
                    else:
                        sum_ += img_0[ir - 1, ic]
                    # out of bounds left
                    if ic == start_c:
                        # boundary conditions taken from img2
                        sum_ += img1[ir, start_c]
                    else:
                        sum_ += img_0[ir, ic - 1]
                    # out of bounds botom
                    if ir == (end_r - 1):
                        # boundary conditions taken from img2
                        sum_ += img1[end_r - 1, ic]
                    else:
                        sum_ += img_0[ir + 1, ic]
                    # out of bounds right
                    if ic == (end_c - 1):
                        # boundary conditions taken from img2
                        sum_ += img1[ir, end_c - 1]
                    else:
                        sum_ += img_0[ir, ic + 1]
                    img_0[ir, ic] = (1 / 4) * (sum_ - div[ir, ic])
                # inside the image
                else:
                    img_0[ir, ic] = (1 / 4) * (
                            img_0[ir + 1, ic] + img_0[ir - 1, ic] + img_0[ir, ic - 1] + img_0[ir, ic + 1] - div[
                        ir, ic])
        print(f'iter #{iter_}')
        # out = 255 * img_0 / np.max(img_0)
        out = img_0
        out = out[:, :, [2, 1, 0]]
        cv2.imwrite(f'kluk_iter_{iter_}.png', out)
        iter_ += 1
        # plt.imshow(img_0 / np.max(img_0))
        # plt.show()

    return img_0


def compose_div(im1, im2, mask):
    dx1, dy1 = im_dx_dy(im1)
    dx2, dy2 = im_dx_dy(im2)
    gx_ = mask * dx1 + (np.logical_not(mask) * dx2)
    gy_ = mask * dy1 + (np.logical_not(mask) * dy2)
    gdx_, _ = im_dx_dy(gx_)
    _, gdy_ = im_dx_dy(gy_)
    return gdx_ + gdy_


def compose_div_hand(im1, im2):
    dx1, dy1 = im_dx_dy(im1)
    dx2, dy2 = im_dx_dy(im2)
    dx1[1900:(1900 + dx2.shape[0]), 1300:(1300 + dx2.shape[1]), :] = dx2
    dy1[1900:(1900 + dy2.shape[0]), 1300:(1300 + dy2.shape[1]), :] = dy2
    cv2.imwrite('dx_ruka.png', dx1)
    cv2.imwrite('dy_ruka.png', dy1)
    gdx_, _ = im_dx_dy(dx1)
    _, gdy_ = im_dx_dy(dy1)
    out = gdx_ + gdy_
    out = 255 * (out - np.min(out)) / (np.max(out) - np.min(out))

    cv2.imwrite('div_ruka.png', out)
    return gdx_ + gdy_


def im_dx_dy(im):
    dx = convolve1d(im, np.array([1, -1]), axis=0)
    dy = convolve1d(im, np.array([1, -1]), axis=1)
    return dx, dy


path = './images'
fnameA = 'klukA.png'
fnameB = 'klukB.png'
# fnameA = 'ruka.jpg'
# fnameB = 'oko.jpg'
imA = cv2.imread(os.path.join(path, fnameA))
# opencv works with BGR
imA = imA[:, :, [2, 1, 0]]
imB = cv2.imread(os.path.join(path, fnameB))
imB = imB[:, :, [2, 1, 0]]

print(imA.shape)
print(imB.shape)

# compose mask...
# mask = np.fliplr(np.tril(np.ones(imA.shape[:2]), 150))
mask = np.ones(imA.shape[:2])
mask[400:550, 740:900] = 0
# mask[850:, :] = 1
# for ir in range(0, 900):
#     mask[ir, (1500 - int(1.75 * ir)):1500] = 1
#     add dimension for elementwise multiplication with images
mask = mask[..., np.newaxis]

# plt.imshow(imB)
# plt.show()


stitch = np.array(imA * mask + (1 - mask) * imB, np.float)
# plt.imshow(stitch / np.max(stitch))
# plt.show()
# stitch = 255 * stitch / np.max(stitch)
stitch = stitch[:, :, [2, 1, 0]]
cv2.imwrite('input_stitch.png', np.uint8(stitch))

imB = np.array(imB, np.float)
imA = np.array(imA, np.float)
# #
out = gauss_seidel(imA, imB, mask, n_iters=400)
# # out = gauss_seidel(imA, imB, mask, n_iters=50, start_r=1900, start_c=1300, end_r=2000 + imB.shape[0],
# #                    end_c=1500 + imB.shape[1])
out = out[:, :, [2, 1, 0]]
cv2.imwrite('out_kluk.png', out)
out = 255 * out / np.max(out)
cv2.imwrite('out_kluk_sc.png', out)
plt.imshow(out / np.max(out))
plt.show()
plt.imshow(out)
plt.show()
