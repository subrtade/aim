import numpy as np
import cv2
import maxflow
from matplotlib import pyplot as plt

# opencv has BGR channels
BLUE = np.array([255, 0, 0])
GREEN = np.array([0, 255, 0])
T = 1000

im = cv2.imread('images/turtle.jpg', cv2.IMREAD_GRAYSCALE)
# print(np.min(im))
# print(np.max(im))
im = im / np.max(im)
labels_suffix = ''
labels = cv2.imread(f'images/turtle_labels{labels_suffix}.png')

# print(labels.shape)


def edge_weight(x, y):
    return 1 + T*min(x, y)**6


g = maxflow.Graph[float](0, 0)

term_cap_source = np.zeros((im.shape[0], im.shape[1]))
term_cap_sink = np.zeros((im.shape[0], im.shape[1]))
# pixel is blue if all bgr components match, axis=2 is the color dimension
# blue for source
term_cap_source[np.all(np.equal(labels, BLUE), axis=2) == True] = T
# green for sink
term_cap_sink[np.all(np.equal(labels, GREEN), axis=2) == True] = T

# source and sink are implicitly detached from grid_nodes
nodeids = g.add_grid_nodes(im.shape[:2])
# add weighted edges
for x in range(im.shape[0]):
    for y in range(im.shape[1]):
        if x < im.shape[0] - 1:
            weight = edge_weight(im[x, y], im[x + 1, y])
            g.add_edge(nodeids[x, y], nodeids[x + 1, y], weight, weight)
        if y < im.shape[1] - 1:
            weight = edge_weight(im[x, y], im[x, y + 1])
            g.add_edge(nodeids[x, y], nodeids[x, y + 1], weight, weight)

# add edges from/to terminals
g.add_grid_tedges(nodeids, term_cap_source, term_cap_sink)

# max flow/ min cut
g.maxflow()
# Get the segments.
sgm = g.get_grid_segments(nodeids)
# print(np.sum(sgm == True))

# The labels should be 1 where sgm is False and 0 otherwise.
mask = np.int_(np.logical_not(sgm))
background = (im * mask).reshape(im.shape[0], im.shape[1], 1)
foreground = (im * np.logical_not(mask)).reshape(im.shape[0], im.shape[1], 1)
output = background * BLUE.reshape(1, 1, 3) + foreground * GREEN.reshape(1, 1, 3)

plt.imshow(output[:, :, [2, 1, 0]])
plt.show()
cv2.imwrite(f'images/output_turtle_labels{labels_suffix}.png', output)
